//Función para limpiar
function limpiar(){
    document.getElementById("respuesta").innerHTML = "";
    document.getElementById("promedioTot").innerHTML = "<label>Promedio Total</label>";
}


//Función para la petición
function cargar(){
    const url = "/html/alumnos.json";

    axios
    .get(url) //Se obtiene la información del sitio a consumir
    .then((res)=>{  //El Axios nos permite obtener la información transformada en Json directamente
        mostrar(res.data)
    })
    .catch((err =>{     //Por si surge algun error en el pedido de los datos
        console.log("Surgio un error" + Error);
    }
    ))

    function mostrar(data){
        //Declaración de variables
        let resp = document.getElementById("respuesta");
        let promedio = 0.0;
        let promedioTot = 0.0;
        let contador = 0;

        //Ciclo de iteración
        for(item of data){
            //Calculo de los promedios
            promedio = (item.matematicas + item.quimica + item.fisica + item.geografia) / 4;
            promedioTot += promedio;

            //Impresión de los datos
            resp.innerHTML += `<tr>
                <td>${item.id}</td>
                <td>${item.matricula}</td>
                <td>${item.nombre}</td>
                <td>${item.matematicas}</td>
                <td>${item.quimica}</td>
                <td>${item.fisica}</td>
                <td>${item.geografia}</td>
                <td>${promedio.toFixed(2)}</td>
            </tr>`;
            contador ++;
        }

        //Impresión del promedio general
        promedioTot /= contador;
        document.getElementById("promedioTot").innerHTML = `<label>Promedio Total ${promedioTot.toFixed(2)}</label>`;
    }
}


//Configuración de botones
document.getElementById("btnLimpiar").addEventListener("click", limpiar);
document.getElementById("btnMostrar").addEventListener("click", function(){
    cargar();
});